const express = require("express");
const logger = require("morgan");
const mongoose = require("mongoose");
const dbURI = require("./config/dbURI");
const authRouter = require("./routes/authRouter");
const usersRouter = require("./routes/usersRouter");
const notesRouter = require("./routes/notesRouter")

PORT = process.env.PORT || 8080;
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger("combined"));

app.use("/api/auth", authRouter);
app.use("/api/users", usersRouter)
app.use("/api/notes", notesRouter)

async function start() {
  try {
    await mongoose.connect(dbURI);
    app.listen(PORT, () => {
      console.log(`Server is listening ${PORT} port`);
    });
  } catch (e) {
    console.log(e);
  }
}
start();
