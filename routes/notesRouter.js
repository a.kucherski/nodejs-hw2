const express = require("express");
const {
  addNote,
  getAllNotes,
  getNote,
  checkNote,
  updateNote,
  deleteNote,
} = require("../controllers/notesController");
const { verifyToken } = require("../controllers/authController");
const router = express.Router();

router.post("/", verifyToken, addNote);
router.get("/", verifyToken, getAllNotes);
router.get("/:id", verifyToken, getNote);
router.patch("/:id", verifyToken, checkNote);
router.put("/:id", verifyToken, updateNote);
router.delete("/:id", verifyToken, deleteNote);

module.exports = router;
