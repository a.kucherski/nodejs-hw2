const Note = require("../models/Note");

async function getAllNotes(req, res) {
  const { offset, limit } = req.query;
  try {
    let notes = await Note.find({ userId: req.userId });

    if (offset && limit) {
      notes = notes.splice(offset, limit);
    }

    res
      .status(200)
      .json({
        offset: +offset || 0,
        limit: +limit || 0,
        count: notes.length,
        notes,
      });
  } catch (e) {
    console.log(e);
    res.status(500).json({ message: " Server error" });
  }
}

async function addNote(req, res) {
  const { text } = req.body;

  if (!text) {
    return res.status(400).json({ message: "No text assigned" });
  }

  try {
    await Note.create({
      userId: req.userId,
      text,
    });
    res.status(200).json({ message: "Success" });
  } catch (e) {
    res.status(500).json({ message: "Server error" });
  }
}

async function getNote(req, res) {
  const { id } = req.params;
  try {
    const note = await Note.findById(id);

    if (!note) {
      return res.status(400).json({ message: "No such note found" });
    }

    res.status(200).json({ note });
  } catch (e) {
    res.status(400).json({ message: "Bad id" });
  }
}

async function checkNote(req, res) {
  const { id } = req.params;
  try {
    const note = await Note.findById(id);
    await Note.updateOne({ _id: id }, { completed: !note.completed });
    res.status(200).json({ message: "Success" });
  } catch (e) {
    res.status(400).json({ message: "Bad id" });
  }
}

async function updateNote(req, res) {
  const { id } = req.params;
  try {
    await Note.updateOne({ _id: id }, { text: req.body.text });
    res.status(200).json({ message: "Success" });
  } catch (e) {
    res.status(400).json({ message: "Bad id" });
  }
}

async function deleteNote(req, res) {
  const { id } = req.params;
  try {
    await Note.deleteOne({ _id: id });
    res.status(200).json({ message: "Success" });
  } catch (e) {
    res.status(400).json({ message: "Bad id" });
  }
}

module.exports = {
  addNote,
  getAllNotes,
  getNote,
  checkNote,
  updateNote,
  deleteNote,
};
