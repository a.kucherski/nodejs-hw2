const {Schema, model} = require("mongoose")

const schema = new Schema({
    userId: String,
    completed: {type: Boolean, default: false},
    text: String,
    createdDate: {type: String, default: new Date().toISOString()}
})

module.exports = model("Note", schema)