const {Schema, model} = require("mongoose")

const schema = new Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    createdDate: {type: String, default: new Date().toISOString()}
})

module.exports = model("User", schema)